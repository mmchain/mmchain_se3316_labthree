$(document).ready(function() {

  $(".button").click(function() {

    var textboxInput = $("#textbox").val();

    if (containsHashTag(textboxInput) >= 0) {
      var hashtagString = "";
      for (var i = containsHashTag(textboxInput); i < textboxInput.length; i++) {
        if (textboxInput[i] == " ")
          break;
        hashtagString += textboxInput[i];
      }

      $.post("/twittr/messageboard", {
        username: $(".username").val(),
        posting: $("#textbox").val(),
        hashtag: hashtagString
      });

    }

    else {
      alert("Your post does not contain a hashtag. Make a post with a hashtag.");
    }

  });

  setInterval(function() {
    updateDisplay("/twittr/database")
  }, 1000);


});


function updateDisplay(url) {

  $.get(url, function(data) {
    $("#RecentPosts").empty();
    for (var i = 0; i < data.length; i++) {
      $("#RecentPosts").append($("<div></div>").append("<u><b>" + data[i].username + ": </b></u><br>"));
      $("#RecentPosts").children().eq(i).addClass("RecentPosts");
      $("#RecentPosts").children().eq(i).append("<b>#</b>" + data[i].hashtag + "<br>");
      $("#RecentPosts").children().eq(i).append(data[i].posting);
    }
    
    
  });


}

function containsHashTag(s) {
  for (var i = 0; i < s.length; i++) {
    if (s[i] == '#')
      return i + 1; //returns the index after the hashtag
  }
  return -1;
}
