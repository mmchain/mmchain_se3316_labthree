var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var MessageSchema = new Schema ({
    
    hashtag: String,
    username: String,
    posting: String
});

module.exports = mongoose.model('Posting', MessageSchema);