var express = require('express');
var app = express();
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

var port = process.env.PORT || 8080;

/*var router = express.Router();
router.get('/', function(req, res) {
    res.json({
        message: 'hooray! welcome to our api!'
    });
    console.log("goop");

}); */


var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/entry'); // connect to our database

var Entry = require('./entry');
var router = express.Router();

router.use(function(req, res, next) {
    console.log('Something is happening.');
    next();
});

router.get('/', function(req, res) {
    res.json({
        message: 'hooray! welcome to our api!'
    });
    console.log(req.url);
});



router.post('/messageboard', function(req, res) {

    var ntry = new Entry();
    ntry.username = req.body.username;
    ntry.posting = req.body.posting;
    ntry.hashtag = req.body.hashtag;

    ntry.save(function(err) {
        if (err)
            res.send(err);

        res.json({
            message: 'message created!'
        });
        console.log('message successfully created! ' + req.url);
    });

});


router.get('/database', function(req, res) {
    Entry.find(function(err, database) {
       
        if (err)
            res.send(err);

        res.send(database);

    }).sort({
        _id: -1
    }).limit(20);
});

router.delete('/clear', function(req, res) {
    Entry.remove(function(err, bear) {
        if (err)
            res.send(err);

        res.json({
            message: 'Successfully deleted'
        });
    });
});

app.use('/twittr', router);
app.use('/', express.static('mainpage'));
app.listen(port);
console.log('Magic happens on port ' + port);
